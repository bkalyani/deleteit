import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// tslint:disable-next-line: class-name
interface response {
  message: string;
  success: boolean;
}
// tslint:disable-next-line: class-name
interface show {
  message: any;
  success: boolean;
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  show() {
    return this.http.get<response>('/api/medicine/medicines');
  }
  showMedicine(id) {
    return this.http.get<show>('/api/medicine/medicines/' + id);
  }
  add(name, price, mDate, eDate, description) {
    return this.http.post<response>('/api/medicine/medicines', {name, price, mDate, eDate, description});
  }
  update(id, name, price, mDate, eDate, description) {
    return this.http.put<response>('/api/medicine/medicines/' + id, {name, price, mDate, eDate, description});
  }
  delete(id) {
    return this.http.delete<response>('/api/medicine/medicines/' + id);
  }
  
}
