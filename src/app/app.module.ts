import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { ShowComponent } from './medicine_details/medicine_details.component';
import {RouterModule} from '@angular/router';
import { AddComponent } from './medicine_form/medicine_form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  declarations: [
    AppComponent,
    ShowComponent,
    AddComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    NgxPaginationModule,
    RouterModule.forRoot([
      {
        path: '',
        component: ShowComponent
      },
      {
        path: 'add',
        component: AddComponent
      },
      {
        path: 'add/:id',
        component: AddComponent
      }
    ]),
    NgbModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
