import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

interface response {
  msg: string;
};

@Component({
  selector: 'app-show',
  templateUrl: './medicine_details.component.html',
  styleUrls: ['./medicine_details.component.scss']
})

export class ShowComponent implements OnInit {


  constructor(private auth: AuthService, private router: Router, private modalService: BsModalService) { }
  medicines = '';
  p = 1;
  itemsPerPage = 1;
  modalRef: BsModalRef;

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    const h = 10;
    console.log('go to home value', h);

  }
  confirm(id): void {
    this.delete(id);
    this.modalRef.hide();
    console.log('tesring pull1');
  }
  decline(): void {
    this.modalRef.hide();
    console.log('we are testing test-2');
  }
  ngOnInit() {
    this.itemsPerPage = 3;
    console.log('ShowComponent');
    this.auth.show().subscribe(data => {
      this.medicines = data.message;
      console.log('Medicines ', this.medicines);
    });

  }
  delete(id) {
    this.auth.delete(id).subscribe(data => {
      if (data.success) {
        this.medicines = data.message;
      }
    });
  }

  test() {
    console.log("test")
  }
}
