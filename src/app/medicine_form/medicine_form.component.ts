import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import {TemplateRef} from '@angular/core';
@Component({
  selector: 'app-add',
  templateUrl: './medicine_form.component.html',
  styleUrls: ['./medicine_form.component.scss']
})
export class AddComponent implements OnInit {

  constructor(private auth: AuthService, private router: Router, private route: ActivatedRoute, private modalService: BsModalService) { }
  add: FormGroup;
  modalRef: BsModalRef;
  id = '';
  task = '';
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id == null) {
    this.task = 'Medicine Information Added Successfully!';
    this.add = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z ]+[0-9]*')]),
      price: new FormControl(null, [Validators.required, Validators.min(0)]),
      mDate: new FormControl(null, [Validators.required, this.mdateCompare.bind(this)]),
      eDate: new FormControl(null, [Validators.required, this.edateCompare.bind(this)]),
      description: new FormControl(null, Validators.required)
    });
  } else {
      this.showMedicine(this.id);
    }
  }

  showMedicine(id) {
    this.task = 'Medicine Information Updated Successfully!';
    this.auth.showMedicine(id).subscribe(data => {
      const medicines = data.message;
      const medicine = medicines[0];
      console.log(medicine.manufacture_date.substr(0, 10));
      this.add = new FormGroup({
        name: new FormControl(medicine.name, [Validators.required, Validators.pattern('[a-zA-Z ]+[0-9]*')]),
        price: new FormControl(medicine.price, [Validators.required, Validators.min(0)]),
        mDate: new FormControl(medicine.manufacture_date.substr(0, 10), [Validators.required, this.mdateCompare.bind(this)]),
        eDate: new FormControl(medicine.expiry_date.substr(0, 10), [Validators.required, this.edateCompare.bind(this)]),
        description: new FormControl(medicine.description, Validators.required)
      });
    });
  }

    addMedicine() {
      const medicine = this.add;
      console.log('Medicine name ' + medicine.get('mDate').value);
      // tslint:disable-next-line: max-line-length
      if (this.id == null) {
        // tslint:disable-next-line: max-line-length
        this.auth.add(medicine.get('name').value, medicine.get('price').value, medicine.get('mDate').value, medicine.get('eDate').value, medicine.get('description').value).subscribe(data => {
        if (data.success) {
          console.log('Inserted');
          this.router.navigate(['']);
        }

      });
    } else {
      // tslint:disable-next-line: max-line-length
    this.auth.update(this.id, medicine.get('name').value, medicine.get('price').value, medicine.get('mDate').value, medicine.get('eDate').value, medicine.get('description').value).subscribe(data => {
      if (data.success) {
        console.log('Updated');
        this.router.navigate(['']);
      }

    });

    }
    }
    mdateCompare(control: FormControl) {
      const dateTime = new Date();
      const dateval = new Date(control.value);
      if (dateval > dateTime) {
        return {compare: true};
      }
      return null;

    }

    edateCompare(control: FormControl) {
      const dateTime = new Date();
      const dateval = new Date(control.value);
      if (dateval < dateTime) {
        return {compare: true};
      }
      return null;

    }

    openModal(template: TemplateRef<any>){
      this.modalRef = this.modalService.show(template);

    }


}
